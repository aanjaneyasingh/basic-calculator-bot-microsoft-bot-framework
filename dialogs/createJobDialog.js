const { ComponentDialog, WaterfallDialog, TextPrompt, ChoicePrompt, ConfirmPrompt } = require('botbuilder-dialogs')

const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

const ROLE_PROMPT = 'ROLE_PROMPT';
const POSITION_PROMPT = 'POSITION_PROMPT';
const CHOICE_PROMPT = 'CHOICE_PROMPT';
const CONFIRM_PROMPT = 'CONFIRM_PROMPT';

class CreateJobDialog extends ComponentDialog {
    constructor(id) {
        super(id || 'createJobDialog');

        this.addDialog(new TextPrompt(ROLE_PROMPT));
        this.addDialog(new ChoicePrompt(POSITION_PROMPT));
        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new ConfirmPrompt(CONFIRM_PROMPT));

        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.roleStep.bind(this),
            this.positionStep.bind(this),
            this.choiceStep.bind(this),
            this.confirmStep.bind(this),
            this.finalStep.bind(this)
        ]));   

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async roleStep(step) {
        console.log(step)
        return step.prompt(ROLE_PROMPT,'What role would you like?')
    }

    async positionStep(step) {
        return step.prompt(POSITION_PROMPT, 'We have position IDs for Store Manager, Regional Store Manager, and Assistant Store Manager, which is most accurate?',['Store Manager','Reg. Store Manager','Ass. Store Manager']);
    }

    async choiceStep(step) {
        return step.prompt(CHOICE_PROMPT,'Is this a permanent role or Contractor?',['Permanent Role','Contractor']);
    }

    async confirmStep(step) {
        await step.context.sendActivity('OK requisition has been created for '+ step.result.value);
        return step.prompt(CONFIRM_PROMPT, 'Do you want to consider internal employees for this role also.')
    }

    async finalStep(step) {
        step.values.confirmation = step.result;
        await step.context.sendActivity('OK! Thanks.');
        
        return await step.endDialog();
    }
}

module.exports.CreateJobDialog = CreateJobDialog ;