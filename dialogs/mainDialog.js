const { CardFactory } = require('botbuilder');
const AdaptiveCard = require('../resources/adaptiveCard.json');

const {
    ChoiceFactory,
    ChoicePrompt,
    ComponentDialog,
    DialogSet,
    DialogTurnStatus,
    TextPrompt,
    WaterfallDialog
} = require('botbuilder-dialogs');

const CHOICE_PROMPT = 'CHOICE_PROMPT';
const TEXT_PROMPT = 'TEXT_PROMPT'; 
const WATERFALL_DIALOG = 'WATERFALL_DIALOG';

class MainDialog extends ComponentDialog {
    constructor(createJobDialog) {
        super('mainDialog');

        this.addDialog(new ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new TextPrompt(TEXT_PROMPT));
        this.addDialog(createJobDialog);

        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.initialStep.bind(this),
            this.choiceStep.bind(this),
        ]));

        this.initialDialogId = WATERFALL_DIALOG;
    }

    async run(turnContext, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(turnContext);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    async initialStep(step) {
        console.log(step.context.activity.value)
        const promptOptions = {
            prompt :'Click below to start calculator',
            choices: ChoiceFactory.toChoices(['Basic Calculator'])
        }

        var value = step.context.activity.value

        if (value) {
            if (value.submit === 'add') {
                if( value.num1 !== '' && value.num2!== '' ) {
                    // var str1 = "First Name : "
                    // var str3 = "last name :"
                    // var res = str1.concat(value.firstName, "\nLast Name : ", value.lastname);
                    var sum = Number(value.num1)+Number(value.num2)
                    var sum1 = sum.toString(10)
                    return await step.context.sendActivity(sum1);
                } 
                else {
                    await step.context.sendActivity('Please Enter the required fields.');
                    return await step.context.sendActivity( {attachments: [CardFactory.adaptiveCard(AdaptiveCard)]}); 
                }
            }
            if (value.submit === 'div') {
                if( value.num1 !== '' && value.num2!== '' ) {
                    // var str1 = "First Name : "
                    // var str3 = "last name :"
                    // var res = str1.concat(value.firstName, "\nLast Name : ", value.lastname);
                    var sum = Number(value.num1)/Number(value.num2)
                    var sum1 = sum.toString(10)
                    return await step.context.sendActivity(sum1);
                } 
                else {
                    await step.context.sendActivity('Please Enter the required fields.');
                    return await step.context.sendActivity( {attachments: [CardFactory.adaptiveCard(AdaptiveCard)]}); 
                }
            }
            if (value.submit === 'sub') {
                if( value.num1 !== '' && value.num2!== '' ) {
                    // var str1 = "First Name : "
                    // var str3 = "last name :"
                    // var res = str1.concat(value.firstName, "\nLast Name : ", value.lastname);
                    var sum = Number(value.num1)-Number(value.num2)
                    var sum1 = sum.toString(10)
                    return await step.context.sendActivity(sum1);
                } 
                else {
                    await step.context.sendActivity('Please Enter the required fields.');
                    return await step.context.sendActivity( {attachments: [CardFactory.adaptiveCard(AdaptiveCard)]}); 
                }
            }
            if (value.submit === 'mul') {
                if( value.num1 !== '' && value.num2!== '' ) {
                    // var str1 = "First Name : "
                    // var str3 = "last name :"
                    // var res = str1.concat(value.firstName, "\nLast Name : ", value.lastname);
                    var sum = Number(value.num1)/Number(value.num2)
                    var sum1 = sum.toString(10)
                    return await step.context.sendActivity(sum1);
                } 
                else {
                    await step.context.sendActivity('Please Enter the required fields.');
                    return await step.context.sendActivity( {attachments: [CardFactory.adaptiveCard(AdaptiveCard)]}); 
                }
            } 
            else {
                await step.context.sendActivity("OK Let's Start Over.");
                return await step.prompt(CHOICE_PROMPT, promptOptions);
            }    
        } 
        else {
            return await step.prompt(CHOICE_PROMPT, promptOptions);    
        }
             
    }

    async choiceStep(step) {
        if (step.result.value === 'Basic Calculator')  {
            await step.context.sendActivity( {attachments: [CardFactory.adaptiveCard(AdaptiveCard)]}); 
            return await step.endDialog();  
           } 
    }
}

module.exports.MainDialog = MainDialog;
